package tourGuide.model;

import gpsUtil.location.Location;

import java.util.UUID;

public class UsersLocations {
    private UUID userId;
    private Location location;

    public UsersLocations(UUID userId, Location location) {
        this.userId = userId;
        this.location = location;
    }

    public String getUserId() {
        return userId.toString();
    }

    public Location getLocation() {
        return location;
    }
}
