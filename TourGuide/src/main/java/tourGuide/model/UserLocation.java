package tourGuide.model;

import gpsUtil.location.VisitedLocation;
import tourGuide.user.User;

public class UserLocation {
    private VisitedLocation visitedLocation;

    private User user;

    // === Constructor ========================================================
    public UserLocation(VisitedLocation visitedLocation, User user) {
        this.visitedLocation = visitedLocation;
        this.user = user;
    }

    // === Getters and setters ================================================
    public VisitedLocation getVisitedLocation() {
        return visitedLocation;
    }

    public void setVisitedLocation(VisitedLocation visitedLocation) {
        this.visitedLocation = visitedLocation;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
