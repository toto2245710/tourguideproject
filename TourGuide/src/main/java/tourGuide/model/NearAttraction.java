package tourGuide.model;

import gpsUtil.location.Attraction;
import gpsUtil.location.Location;

public class NearAttraction extends Attraction {
    private Location userLocation;
    private int rewardPoints;
    private double distanceInMiles;

    public NearAttraction(Attraction attraction, double distanceInMiles, Location userLocation, int rewardPoints) {
        super(attraction.attractionName, attraction.city, attraction.state, attraction.latitude, attraction.longitude);
        this.distanceInMiles = distanceInMiles;
        this.rewardPoints = rewardPoints;
        this.userLocation = userLocation;

    }
}
